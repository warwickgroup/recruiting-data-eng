Warwick - Data Engineering Challenge
====================================

The challenge below is designed to expose a core understanding of data
engineering principles.

### Objective:

Create a dataset and accompanying transformations in the data and transformation platform of your choosing 
### Guidelines:

-   Create a staging dataset with FracFocus data (landing zone for DE)
    -   (FracFocus is a public data set available at [https://fracfocus.org/](https://fracfocus.org/))

-   Create a reporting dataset, transforming the Fracfocus staging data into a Well Treatment reporting dataset with the following facts:

    -   Number of jobs started by supplier by well by month

    -   Max and Min Percentage of Ingredients used per Well

    -   Total Base Water Volume by State, County and Well

-   Add another data source OCC [(ftp://ftp.occeweb.com/OG_DATA/W27base.zip)](ftp://ftp.occeweb.com/OG_DATA/W27base.zip) to the reporting dataset data:
    -   Only Oklahoma data is required
    -   Only attributes Well Name, API Number, Well Status, Spud Date, First Production Date

-   Create a query for reporting to integrate the FracFocus header table with the OCC data
 &nbsp;
-   Discuss
	- Different tools for data platform, ETL/ELT, and cloud
	- Scheduling the process
    - Incremental updates to data sources
	- Data Modeling techniques
	- Data Quality
    - Testing the url in the command line

### Timeline and discussion:

-   This task should be completed in 5 days

-   After the task is complete we will schedule a discussion so that you can
    walk us through your approach


### Notes

Communication is encouraged. If you have questions about the technical specs,
data or the oil and gas industry don't hesitate to contact us while you are
doing this exercise

[sam@warwickinvestmentgroup.com](sam@warwickinvestmentgroup.com)

This solution can be built on a personal machine or in a cloud service.  If you
don't have any of these readily available please reach out with your technology
preference and we can discuss a solution.