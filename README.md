![image](/img/logo.PNG)

Warwick Group Recruiting Assessment
-----------------------------------

Thank you for taking the time to do our recruiting assessment. There are three
parts to the assessment that you must complete and submit.

-   [Personality Evaluation](<https://16personalities.com/>)

-   [Free Response Question](#free-response-question)

-   [Data Engineering
    Challenge](https://bitbucket.org/warwickgroup/recruiting-data-eng/src/master/DE%20Exercise/)

We would like you to complete your assessment by uploading all of your work to a
public repository entitled
**{yourfullname}-{preferredlocation}-{roleappliedfor}** and provide a link for
us to view your submission. In your repository, please also include the most
recent version of your **resume** with links to portfolio if available.

We want you to get used to working under time constraints early. You must
complete and submit your assessment to a repository and notify us of completion
with the link within **5 days** of starting the assessment.

Personality Test
----------------

We want to know you as a person. Our ethos drives us to create awesome products
as a team. We ask that you take a personality test at
[16personalities.com](<https://16personalities.com/>). At the end of the test,
you will have the option to share your results with us. Instead of providing our
email, please in the final submission provide a link to the personality type.

In a short paragraph, you may also provide color commentary to the personality
test outcome.

Free Response Question
----------------------

In a paragraph, describe the work environment or culture in which you are the
most happy, productive and passionate and why. This gives us ideas on how we can
best accommodate and welcome you!

Data Engineering Challenge
--------------------------

While MSSQL will be a fundamental platform for this role, during the challenge
feel free to use any platform you like. However, be prepared to discuss why you
chose a certain platform over another. This solution can be built on a personal
machine or in a cloud service. If you don't have any of these readily available
please reach out with your technology preference and we can discuss a solution.
Please present your code in a clear and concise format. And, please be prepared
to discuss your approach and what you might have done differently if you had
more time or resources.

Questions?
----------

If you have any questions for us, please reach out to the assigned contact on
our team.

In the meantime, if you would like to learn more about the company, please visit
the following resources.

Press: [Fortune](http://fortune.com/2017/05/12/warwick-energy-kate-richard/),
[CNBC](https://www.cnbc.com/2017/03/14/opec-us-shale-showdown-biggest-oil-risk-of-2017-says-energy-investor.html),
[Bloomberg](https://www.bloomberg.com/news/videos/2017-11-08/richard-says-we-are-heating-up-for-major-m-a-cycle-video)

What to Expect After Submitting Your Work
---------------------------------------------

You may receive an in-person interview invitation; we have offices in 
Oklahoma City, London, Hong Kong, and New York City. 
If you cannot meet us in person, we may be able to accommodate a remote interview 
through web conferencing. Expect a 10 minute qualitative interview and 40 minute 
technical interview with a small group of interviewers. The interviewer(s) will be 
equally interested in your personality
and knowledge of SQL, data models, data structures, and data warehousing.

### Interview Tips 

-  Plan Ahead

Interview topics may cover anything on your resume, whiteboard coding questions,
current or previous passion projects, building and developing data models and
optimization characterization. 

-  Be Radically Transparent

At Warwick, we believe in the concept of radical transparency, originally
espoused by Ray Dalio and his company Bridgewater Associates, the largest hedge
fund in the world. In short, we are big on sharing ideas and collaborating. If
there is anything you don't understand, it is okay to interact with your
interviewer for help or clarification. Please describe how you want to tackle
solving each part of the question, and always let your interviewer know what you
are thinking as they are interested in your thought process and how you
approach the problem as well as the solution itself.

-  Practice Creativity and Critical Thinking

Many of the questions asked in interviews are deliberately underspecified
because we are looking to see how you interact with the problem. In particular,
we are looking to see which areas leap to mind as the most important piece
of the problem you've been presented.

We look forward to meeting you!
-------------------------------

-   Warwick Team
